package com.project.ciceronenav.di

import com.project.ciceronenav.MainActivity
import com.project.ciceronenav.SampleFragment
import com.project.ciceronenav.SecondFragment
import com.project.ciceronenav.di.module.NavigationModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [
    NavigationModule::class]
)

interface AppComponent {
    fun injectMain(activity: MainActivity)

    fun injectSample(fragment: SampleFragment)

    fun injectSecond(fragment: SecondFragment)
}