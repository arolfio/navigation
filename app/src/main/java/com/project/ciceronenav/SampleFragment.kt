package com.project.ciceronenav

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import com.project.ciceronenav.R
import com.project.ciceronenav.Screens
import com.project.ciceronenav.di.DaggerAppComponent
import com.project.ciceronenav.presenter.SamplePresenter
import com.project.ciceronenav.presenter.SampleView
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SampleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SampleFragment : SampleView, MvpAppCompatFragment() {



    private lateinit var button: Button
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    @Inject
    lateinit var  router: Router

    @Inject
    lateinit var navigatorHolder: NavigatorHolder


    @InjectPresenter
    lateinit var presenter: SamplePresenter

    @ProvidePresenter
    fun createSamplePresenter() = SamplePresenter(router)

    override fun onCreate(savedInstanceState: Bundle?) {
        val dagger = DaggerAppComponent.builder().build()
        dagger.injectSample(this)
        super.onCreate(savedInstanceState)

    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_sample, container, false)
        button = view.findViewById(R.id.button123)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button.setOnClickListener {
            router.replaceScreen(Screens.Second())
        }
    }

    companion object {
        fun getNewInstance(): SampleFragment {
            return SampleFragment()
        }
    }

    override fun setTitle(title: String) {
        TODO("Not yet implemented")
    }
}