package com.project.ciceronenav

import com.github.terrakok.cicerone.androidx.FragmentScreen

object Screens {
    fun Sample() = FragmentScreen {
        SampleFragment()
    }

    fun Second() = FragmentScreen {
        SecondFragment()
    }
}