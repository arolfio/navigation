package com.project.ciceronenav.presenter

import com.github.terrakok.cicerone.Router
import com.project.ciceronenav.Screens.Second
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class SamplePresenter(
    private val router: Router
) : MvpPresenter<SampleView>() {


    fun onBackCommandClick() {
        router.exit()
    }

    fun onForwardCommandClick() {
        router.navigateTo(Second())
    }
}