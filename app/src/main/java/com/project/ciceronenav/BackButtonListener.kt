package com.project.ciceronenav

interface BackButtonListener {
    fun onBackPressed(): Boolean
}